import styled from "styled-components";
import props from "@app/ui/props";
import DefaultButton from "@app/ui/Button";
import InputText from "@app/ui/Input";

export const Button = styled(DefaultButton)`
  background-color: black;
  display: block;
  padding: ${props.sizes.xs} ${props.sizes.l};
  border-radius: ${props.sizes.l};
`

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  margin: 0 auto;
  width: 100%;
  align-self: center;
  position: relative;
  max-width: 500px;
  top: -3rem; // Seems better to the eye :) 
`

export const Form = styled.form`
  display: flex;
  flex: 1 1 auto;
  flex-direction: column;
  
  @media (min-width: 768px) {
    flex-direction: row;
  }
`

export const Input = styled(InputText)`
  margin-right: ${props.sizes.m};
  margin-bottom: ${props.sizes.m};
  flex: 1 1 auto;
  
  @media (min-width: 768px) {
    margin-bottom: 0;
  }
`;

export const P = styled.p`
  margin-top: ${props.sizes.l};
  line-height: 1.3;
  text-align: center;
  color: ${props.gray40};
`

export const Highlight = styled.span`
  font-weight: bold;
  color: ${props.gray20};
`

export const Error = styled.p`
  position: absolute;
  border: 1px solid ${props.red50};
  background-color: ${props.red80};
  color: ${props.red50};
  padding: ${props.sizes.m};
  opacity: 1;
  bottom: 100%;
  left: 0;
  right: 0;
  margin-bottom: ${props.sizes.m};
  border-radius: ${props.radiusS};
  opacity: 1;
  visibility: visible;
  transition: all 0.25s ease-in-out;
  text-align: center;
  line-height: 1.3;
  
  &:empty {
    opacity: 0;
    visibility: hidden;
  }
`