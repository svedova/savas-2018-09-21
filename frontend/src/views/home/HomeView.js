import React, { PureComponent } from "react";
import ReactDOM from "react-dom";
import PropTypes from "prop-types";
import Context from "@app/context/global";
import { LS_JWT } from "@app/constants";
import * as styles from "./HomeView.styles";

const { Container, Button, Input, Form, P, Highlight, Error } = styles;

class HomeView extends PureComponent {
  static propTypes = {
    request: PropTypes.object,
    history: PropTypes.object // Injected by react-router
  };

  state = {
    email: "john.doe@example.com",
    password: "123456"
  };

  componentDidMount() {
    // Since I am using a styled component the ref.current on the component
    // would return the styled component itself, which has no focus method.
    // So the dirty hack..
    ReactDOM.findDOMNode(this)
      .querySelector("input")
      .focus();
  }

  login = async e => {
    e.preventDefault();

    const { request, history } = this.props;

    try {
      const data = await request.post("/auth/login", {
        email: this.state.email,
        password: this.state.password
      });

      if (data.error) {
        return this.setState({
          error:
            "Invalid authentication. Hint: john.doe@example.com/123456 (lol - I hope you won't decide not to hire me because of this)"
        });
      } else {
        localStorage.setItem(LS_JWT, data.access_token);
        history.push("/docs");
      }
    } catch (e) {}
  };

  render() {
    const { email, password, error } = this.state;

    return (
      <Container>
        <Form onSubmit={this.login}>
          <Input
            type={"text"}
            value={email}
            onChange={e =>
              this.setState({ email: e.target.value, error: null })
            }
          />
          <Input
            type={"password"}
            value={password}
            onChange={e =>
              this.setState({ password: e.target.value, error: null })
            }
          />
          <Button type={"submit"}>login</Button>
        </Form>
        {/* Normally I would not even render the error component when empty -
            but it was easier for transition, didn't want to implement animations */}
        <Error>{error}</Error>
        <P>
          If you have followed the Readme correctly, you should have a user
          already setup for you in the database. By click to{" "}
          <Highlight>login</Highlight>, the application will create a JWT token
          for you and will store it to your local storage.
        </P>
      </Container>
    );
  }
}

export default Object.assign(
  props => (
    <Context.Consumer
      children={({ request }) => <HomeView {...props} request={request} />}
    />
  ),
  {
    displayName: "withContext(HomeView)"
  }
);
