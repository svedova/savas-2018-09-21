import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import Dropzone from "react-dropzone";
import { connect } from "react-redux";
import Context from "@app/context/global";
import { fetchDocs, deleteDoc, uploadFiles } from "./store/actions";
import * as Styles from "./DocsView.styles";

const {
  Wrapper,
  DocsWrapper,
  DropzoneWrapper,
  DropzonePH,
  NoDocs,
  Files,
  File,
  Title,
  Button,
  CancelButton,
  ButtonWrapper
} = Styles;

class DocsView extends PureComponent {
  static propTypes = {
    request: PropTypes.object,
    fetchDocs: PropTypes.func, // redux
    deleteDoc: PropTypes.func, // redux
    uploadFiles: PropTypes.func, // redux
    docs: PropTypes.array // redux
  };

  state = {
    files: [],
    fetching: true
  };

  /**
   * List files on drop.
   *
   * @param newFiles
   */
  onDrop = newFiles => {
    this.setState(({ files }) => ({ files: [...files, ...newFiles] }));
  };

  /**
   * Upload the files in the list.
   *
   * @param e
   */
  uploadFiles = async e => {
    e.preventDefault();

    const { request, uploadFiles } = this.props;
    const { files } = this.state;
    const resp = await uploadFiles(request, files);

    if (resp && resp.success !== true) {
      alert("[Again! Sorry for the design] Something went wrong while uploading. Files too big maybe?");
    } else {
      this.setState({ files: [] });
    }
  };

  /**
   * Remove a file from the upload list.
   *
   * @param i
   */
  removeFile = i => {
    return () => {
      const { files } = this.state;
      const clone = files.slice(0);
      clone.splice(i, 1);

      // The filter is used to remove the empty value
      // which remains after splicing.
      this.setState({ files: clone.filter(i => i) });
    };
  };

  /**
   * Empty the files to be uploaded.
   */
  emptyFiles = () => {
    this.setState({ files: [] });
  };

  /**
   * Make a request to the api to delete a certain file.
   *
   * @param title
   * @param id
   * @param i
   */
  deleteFile = (title, id, i) => {
    return async () => {
      const p = global.confirm(
        `[Sorry about the design, would be more appropriate to use a modal here] Are you sure you want to delete ${title}?` // prettier-ignore
      );

      if (p) {
        const { request, deleteDoc } = this.props;
        const resp = deleteDoc(request, id, i);

        if (resp && resp.success !== true) {
          // TODO: Display user an error message.
        }
      }
    };
  };

  async componentDidMount() {
    const { request, fetchDocs } = this.props;
    await fetchDocs(request);
    this.setState({ fetching: false });
  }

  /**
   * Render a list of files to upload.
   *
   * @param files
   * @return {*}
   */
  renderFilesToUpload = files => (
    <Files>
      <Title>Files to upload</Title>
      {files.map((f, i) => (
        <File key={f.name}>
          <span className={"text"}>{f.name}</span>
          <span className={"fas fa-trash"} onClick={this.removeFile(i)} />
        </File>
      ))}
      <ButtonWrapper>
        <CancelButton onClick={this.emptyFiles}>Cancel</CancelButton>
        <Button onClick={this.uploadFiles}>Upload files</Button>
      </ButtonWrapper>
    </Files>
  );

  /**
   * Render a list of already uploaded files.
   *
   * @param files
   */
  renderUploadedFiles = files => (
    <Files>
      <Title>Uploaded files</Title>
      {files.map((f, i) => (
        <File key={f.id}>
          <span className={"text"}>#{f.id} - {f.title}</span>
          <span className={"fas fa-trash"} onClick={this.deleteFile(f.title, f.id, i)} />
        </File>
      ))}
    </Files>
  );

  render() {
    const { fetching, files } = this.state;
    const { docs } = this.props;

    if (fetching) {
      return <Wrapper>...</Wrapper>;
    }

    const isEmpty = !docs.length && !files.length;

    return (
      <Wrapper>
        <DropzoneWrapper>
          <Dropzone
            onDrop={this.onDrop}
            accept=".jpeg,.jpg,.png,.pdf,.txt,.doc"
          >
            <DropzonePH>
              Try dropping some files here, or click to select files to upload.
            </DropzonePH>
          </Dropzone>
        </DropzoneWrapper>
        <DocsWrapper>
          {isEmpty && <NoDocs>You have no docs yet.</NoDocs>}
          {!!docs.length && this.renderUploadedFiles(docs)}
          {!!files.length && this.renderFilesToUpload(files)}
        </DocsWrapper>
      </Wrapper>
    );
  }
}

const enhanced = props => (
  <Context.Consumer
    children={({ request }) => <DocsView {...props} request={request} />}
  />
);

const mapStateToProps = state => ({
  docs: state.docs && state.docs.all
});

export default connect(
  mapStateToProps,
  { fetchDocs, deleteDoc, uploadFiles }
)(enhanced);
