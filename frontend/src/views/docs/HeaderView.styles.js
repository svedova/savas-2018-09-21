import styled from "styled-components";
import Input from "@app/ui/Input";
import { props } from "@app/ui";

export const Wrapper = styled.div`
  display: flex;
  flex: 1 1 auto;
  justify-content: flex-end;

  @media (min-width: 1024px) {
    position: absolute;
    left: 50%;
    top: 50%;
    transform: translate(-50%, -50%);
    width: 500px;
    z-index: 1000;    
  }
`;

export const InnerWrapper = styled.div`
  flex: 1 1 auto;
  position: relative;
  display: flex;
`;

export const Search = styled(Input)`
  flex: 1 1 auto;
  border: 1px solid ${props.gray70};
  padding: ${props.sizes.xs};
  border-radius: ${props.radiusS};
  margin-left: ${props.sizes.l};
  max-width: 500px;
`;

export const Dropdown = styled.div`
  position: absolute;
  top: 100%;
  margin-top: ${props.sizes.m};
  left: 0;
  right: 0;
  padding: ${props.sizes.m};
  z-index: 1;
  background: white;
  box-shadow: ${props.shadowM};
`;

export const Doc = styled.div`
  padding: ${props.sizes.m};
`;

export const CloseIcon = styled.span`
  font-size: 1.4rem;
  position: absolute;
  top: ${props.sizes.m};
  right: ${props.sizes.m};
  cursor: pointer;
`;