import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import {
  Wrapper,
  Search,
  Dropdown,
  InnerWrapper,
  Doc,
  CloseIcon
} from "./HeaderView.styles";
import Context from "@app/context/global";

class HeaderView extends PureComponent {
  static propTypes = {
    request: PropTypes.object
  };

  state = { docs: [] };

  handleKeyPress = async e => {
    if (e.key === "Enter") {
      const docs = await this.props.request.post("/docs/search", {
        title: e.target.value
      });

      this.setState({ docs });
    }
  };

  render() {
    const { docs } = this.state;

    return (
      <Wrapper>
        <InnerWrapper>
          <Search
            placeholder={"Search your documents"}
            onKeyPress={this.handleKeyPress}
          />
          {!!docs.length && (
            <Dropdown>
              <CloseIcon
                className={"fas fa-times"}
                onClick={() => this.setState({ docs: [] })}
              />
              {docs.map(d => (
                <Doc key={d.id}>
                  #{d.id} - {d.title}
                </Doc>
              ))}
            </Dropdown>
          )}
        </InnerWrapper>
      </Wrapper>
    );
  }
}

export default props => (
  <Context.Consumer
    children={({ request }) => <HeaderView {...props} request={request} />}
  />
);
