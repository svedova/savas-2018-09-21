import { FETCH_DOCS, REMOVE_DOC, RESTORE_DOCS } from "./action_types";

/**
 * Action to fetch docs from the database.
 *
 * @param request
 * @return {Function}
 */
export const fetchDocs = request => async dispatch => {
  try {
    const docs = await request.get("/docs");

    if (docs) {
      dispatch({ type: FETCH_DOCS, docs });
    }

    return docs;
  } catch (e) {}
};

/**
 * Action to delete a document from the database.
 *
 * @param request
 * @param id The document id.
 * @param i The document index in the docs state.
 * @return {Function}
 */
export const deleteDoc = (request, id, i) => async (dispatch, getState) => {
  try {
    const { all } = getState().docs;

    // Delete the row immediately - so the user doesn't wait
    // the server response.
    dispatch({ type: REMOVE_DOC, index: i });

    const resp = await request.delete(`/docs/${id}`);

    if (resp && resp.success !== true) {
      dispatch({ type: RESTORE_DOCS, docs: all });
      return { error: true };
    }
  } catch (e) {}
};

/**
 * Upload files to the server.
 *
 * @param request
 * @param files
 * @return {Function}
 */
export const uploadFiles = (request, files) => async dispatch => {
  try {
    const data = new FormData();
    files.forEach(file => {
      data.append("doc[]", file, file.name);
      data.append("title[]", file.name);
    });

    const resp = await request.post("/docs/upload", data);

    if (resp.success) {
      dispatch({ type: FETCH_DOCS, docs: resp.docs });
    }

    return resp;
  } catch (e) {
    console.log(e.message);
  }
};
