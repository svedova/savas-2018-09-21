import * as types from "./action_types";

const defaultState = {
  all: [],
  ids: {}
};

export default (state = defaultState, action) => {
  switch (action.type) {
    // Fetch the docs and append to the list.
    // NOTE: Currently load more feature is not supported
    // but would be easy with this reducer.
    case types.FETCH_DOCS: {
      // Remove duplicate items
      const ids = state.ids;
      const all = state.all.slice(0);

      action.docs.forEach(doc => {
        if (ids[doc.id] === undefined) {
          ids[doc.id] = doc;
          all.push(doc);
        }
      });

      return {
        ...state,
        ids,
        all
      };
    }

    // When something goes wrong, restore the docs.
    case types.RESTORE_DOCS: {
      return {
        ...state,
        all: action.docs
      };
    }

    // Remove a doc from the list.
    case types.REMOVE_DOC: {
      const ids = { ...state.ids };
      const clone = state.all.slice(0);
      const doc = clone[action.index];
      clone.splice(action.index, 1);

      if (doc) {
        delete ids[doc.id];
      }

      return {
        ...state,
        ids,
        all: clone
      };
    }

    default: {
      return state;
    }
  }
};
