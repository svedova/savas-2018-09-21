import styled from "styled-components";
import { props } from "@app/ui";
import DefaultButton from "@app/ui/Button";

export const Wrapper = styled.div`
  margin: ${props.sizes.l} auto;
  display: flex;
  flex-direction: column;
  max-width: 1024px;
  flex: 1 1 auto;
  
  @media (min-width: 768px) {
    flex-direction: row;
  }
`;

export const DropzoneWrapper = styled.div`
  display: flex;
  flex: 1 1 auto;
  cursor: pointer;
  background: white;
  margin-bottom: ${props.sizes.l};
  min-height: 7rem;

  & > div {
    width: auto !important;
    height: auto !important;
    flex: 1 1 auto;
  }
  
  @media (min-width: 768px) {
    max-width: 50%;
    margin-bottom: 0;
    margin-right: ${props.sizes.m};
  }
`;

export const DropzonePH = styled.div`
  position: absolute;
  top: 50%;
  left: 1rem;
  transform: translateY(-50%);
  right: 1rem;
  text-align: center;
  line-height: 1.3;
  font-size: 1.5rem;
  color: ${props.gray30};
`;

export const DocsWrapper = styled.div`
  display: flex;
  flex: 1 1 auto;
  flex-direction: column;
  position: relative;
  
  @media (min-width: 768px) {
    max-width: 50%;
    margin-left: ${props.sizes.m};
  }
`;

export const NoDocs = styled.div`
  position: absolute;
  left: 50%;
  top: 50%;
  transform: translate(-50%, -50%);
  align-self: auto;
  font-size: 1.5rem;
  width: 100%;
  text-align: center;
  color: ${props.gray30};
`;

export const Files = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  margin-bottom: ${props.sizes.l};
`;

export const File = styled.div`
  padding: ${props.sizes.s};
  border-bottom: 1px solid ${props.gray70};
  display: flex;

  &:nth-child(even) {
    background-color: ${props.gray90};
  }

  .text {
    flex: 1 1 auto;
  }

  .fas {
    flex: 0 0 auto;
    color: ${props.red50};
    opacity: 0.3;
    cursor: pointer;

    &:hover {
      opacity: 1;
    }
  }
`;

export const Title = styled.span`
  display: block;
  font-size: 1.2rem;
  color: ${props.gray30};
  margin-bottom: ${props.sizes.s};
`;

export const Button = styled(DefaultButton)`
  padding: ${props.sizes.xs};
  margin-top: ${props.sizes.s};
  flex: 1 1 auto;
  margin-left: ${props.sizes.m};
`;

export const CancelButton = styled(Button)`
  background: ${props.gray40};
  margin-left: 0;
  margin-right: ${props.sizes.m};
`;

export const ButtonWrapper = styled.div`
  display: flex;
`;
