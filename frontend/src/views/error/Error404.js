import React from "react";

const Error404 = () => (
  <React.Fragment>
    <h1>Whoops! Nothing here...</h1>
    <a href={"/"}>Go back to home page</a>
  </React.Fragment>
);

export default Error404;
