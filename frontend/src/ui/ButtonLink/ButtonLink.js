import React from "react";
import styled from "styled-components";
import { props } from "@app/ui";

const ButtonLink = props => <a {...props} />;

export default styled(ButtonLink)`
  display: inline-block;
  background: black;
  color: white;
  padding: ${props.sizes.m} ${props.sizes.xl};
  border-radius: ${props.radiusS};
  cursor: pointer;
  transition: all 0.275s ease-in;
  outline: none;
  border: none;
  text-decoration: none;
  text-align: center;

  &:hover {
    background-color: ${props.gray20};
    box-shadow: ${props.shadowM};
  }
`;
