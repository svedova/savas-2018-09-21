export default {
  sizes: {
    xs: "0.5rem",
    s: "0.75rem",
    m: "1rem",
    l: "2rem",
    xl: "4rem"
  },

  gray20: "#2a2b2c",
  gray30: "#a4a4a4",
  gray40: "#b2b2b2",
  gray50: "#c9c9c9",
  gray70: "#dedede",
  gray90: "#f9f9f9",

  blue50: "#528dc8",

  red50: "#ff3847",
  red80: "#ffb2b0",

  shadowS: "0 3px 2px -2px #c9c9c9",
  shadowM: "0 10px 6px -6px #777",

  radiusS: "3px",
  radiusM: "10px",
};
