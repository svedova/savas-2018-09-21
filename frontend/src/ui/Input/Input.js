import styled from "styled-components";
import props from "../props";

export default styled.input`
  outline: none;
  border: none;
  border-bottom: 1px solid ${props.gray50};
  background: transparent;
  font-size: 0.9rem;
`