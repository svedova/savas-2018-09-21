import React from "react";
import styled from "styled-components";
import { props } from "@app/ui";

const Button = props => <button {...props} />;

export default styled(Button)`
  background: black;
  color: white;
  padding: ${props.sizes.m} ${props.sizes.xl};
  border-radius: ${props.radiusS};
  cursor: pointer;
  transition: all 0.275s ease-in;
  outline: none;
  border: none;
  font-size: 0.9rem;
  
  &:hover {
    background-color: ${props.gray20};
    box-shadow: ${props.shadowM};
  }
`;
