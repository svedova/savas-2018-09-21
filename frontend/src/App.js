import React, { Component } from "react";
import PropTypes from "prop-types";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import { Provider } from "react-redux";
import routes, { headerRoutes } from "./routes";
import { Logo, Header, Container, Wrapper } from "./App.styles";
import Context from "./context/global";
import Request from "./api/Request";

class App extends Component {
  static propTypes = {
    store: PropTypes.object
  };

  request = new Request({ baseurl: process.env.API_DOMAIN });

  render() {
    const { store } = this.props;

    return (
      <Context.Provider value={{ request: this.request }}>
        <Router>
          <Provider store={store}>
            <Wrapper>
              <Header>
                <div>
                  <Link to={"/"}>
                    <Logo
                      src={"https://www.kraken.com/img/facade/kraken_logo.png"}
                    />
                  </Link>
                </div>
                <Switch>
                  {headerRoutes.map(r => (
                    <Route {...r} key={r.path} />
                  ))}
                </Switch>
              </Header>
              <Container>
                <Switch>
                  {routes.map(r => (
                    <Route {...r} key={r.path} />
                  ))}
                </Switch>
              </Container>
            </Wrapper>
          </Provider>
        </Router>
      </Context.Provider>
    );
  }
}

export default App;
