import styled from "styled-components";
import { props } from "./ui";

export const Logo = styled.img`
  display: inline-block;
  width: 10rem;
`;

export const Header = styled.div`
  display: flex;
  background-color: white;
  padding: ${props.sizes.l};
  border-bottom: 1px solid ${props.gray70};
  box-shadow: 0 1px 400px rgba(0, 0, 0, 0.1), 0 0 40px rgba(0, 0, 0, 0.1);
  align-items: center;
  position: relative;
`;

export const Container = styled.div`
  display: flex;
  margin: 0 ${props.sizes.l};
  flex: 1 1 auto;
`;

export const Wrapper = styled.div`
  display: flex;
  flex: 1 1 auto;
  flex-direction: column;
  min-height: 100%;
`;
