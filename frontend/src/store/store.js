import { createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import reducers from "./reducers";

// In SSR environments this would be a function which creates
// a new store since we need a separate store for each request.
export default createStore(reducers, applyMiddleware(thunk));
