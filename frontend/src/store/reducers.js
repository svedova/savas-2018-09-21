import { combineReducers } from "redux";
import docs from "@app/views/docs/store/reducers";

export default combineReducers({ docs })