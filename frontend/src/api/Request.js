import { LS_JWT } from "../constants";

export default class Request {
  constructor(opts) {
    this.baseurl = opts.baseurl;
    this.headers = {
      Accept: "application/json"
    };
  }

  /**
   * Make a GET request to the api.
   *
   * @param url
   * @param params
   * @return {Promise<*>}
   */
  async get(url, params = {}) {
    const qs = Object.keys(params)
      .map(k => encodeURIComponent(k) + "=" + encodeURIComponent(params[k]))
      .join("&");

    // Ugly but works: Since fetch misses the functionality to pass
    // query params as an object, we have to come up with a solution like this.
    const endpoint = `${url}${qs ? `?${qs}` : ""}`;
    return this._req(endpoint, "GET");
  }

  /**
   * Make a POST request to the api.
   *
   * @param url
   * @param params
   * @return {Promise<any>}
   */
  post(url, params = {}) {
    return this._req(url, "POST", params);
  }

  delete(url, params = {}) {
    return this._req(url, "DELETE", params);
  }

  /**
   * Helper function to make requests.
   *
   * @param url
   * @param method
   * @param params
   * @return {Promise<*>}
   * @private
   */
  async _req(url, method, params) {
    const headers = {
      ...this.headers,
      Authorization: "Bearer " + localStorage.getItem(LS_JWT)
    };

    if (!(params instanceof FormData)) {
      headers["Content-Type"] = "application/json";
    }

    const resp = await fetch(this.baseurl + url, {
      method,
      headers,
      body: params instanceof FormData ? params : JSON.stringify(params),
      mode: "cors"
    });

    if (resp.status === 401) {
      localStorage.clear();
      return (window.location.href = "/");
    }

    return await resp.json();
  }
}
