import React, { Component } from "react";

export default getComponent =>
  class AsyncComponent extends Component {
    state = { Component: null };

    async componentDidMount() {
      if (!this.state.Component) {
        const { default: Component } = await getComponent();
        this.setState({ Component });
      }
    }

    render() {
      const { Component } = this.state;

      if (Component) {
        return <Component {...this.props} />;
      }

      return null;
    }
  };
