import { createContext } from "react";

const context = createContext({
  /**
   * The request object which will be used
   * to make api calls. It needs to be initialized
   * by the App component with default values such
   * as baseurl.
   */
  request: null
});

export default context;