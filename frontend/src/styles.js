import { injectGlobal } from "styled-components";
import props from "./ui/props";

injectGlobal`
  /* http://meyerweb.com/eric/tools/css/reset/ 
     v2.0 | 20110126
     License: none (public domain)
  */
  
  html, body, div, span, applet, object, iframe,
  h1, h2, h3, h4, h5, h6, p, blockquote, pre,
  a, abbr, acronym, address, big, cite, code,
  del, dfn, em, img, ins, kbd, q, s, samp,
  small, strike, strong, sub, sup, tt, var,
  b, u, i, center,
  dl, dt, dd, ol, ul, li,
  fieldset, form, label, legend,
  table, caption, tbody, tfoot, thead, tr, th, td,
  article, aside, canvas, details, embed, 
  figure, figcaption, footer, header, hgroup, 
  menu, nav, output, ruby, section, summary,
  time, mark, audio, video {
    margin: 0;
    padding: 0;
    border: 0;
    font-size: 100%;
    font: inherit;
    vertical-align: baseline;
  }
  /* HTML5 display-role reset for older browsers */
  article, aside, details, figcaption, figure, 
  footer, header, hgroup, menu, nav, section {
    display: block;
  }
  body {
    line-height: 1;
  }
  ol, ul {
    list-style: none;
  }
  blockquote, q {
    quotes: none;
  }
  blockquote:before, blockquote:after,
  q:before, q:after {
    content: '';
    content: none;
  }
  table {
    border-collapse: collapse;
    border-spacing: 0;
  }
  
  html {
    min-height: 100%;
    display: flex;
  }
  
  body {
    background:#f8f8f8 url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwAgMAAAAqbBEUAAAACVBMVEUAAAD8+vz09vRl/HpzAAAAAXRSTlMAQObYZgAAAWlJREFUKJEVkkEKBDEIBP1kAvE+gv5HQe8K+sp1rxLa6jIgc9s8QosvjON8WNImCcJfGYl4x0BGWbHK8WFwvOVqZkkDWo8HRyxRgXI2p+hGD4Q8qtJPqAZGMYZCyrFArmfJ2CQ7NN6jtwbTFVhmUNodpYAj6Mw3Zd7QIZ+NHUylTdsxMsZCAGcQfSHndIPGQf6kioW2gortS98hJDcuyWA1wxsi2hS063COc3/4dp2AlOhB7TOhMNmLvVAlBnm1Z1wmtsLt7/m9zyUehJ1MOe32CbhgP9UtTv03eka4y5dNblXWGef1dllts9hav9UrTSxdKQr2krYYd57FiTWBGGuroPvGBG/ePKipWcNrKAx8l1xfJ2PxL+fn8Jt5662pVhCS3a29kjeIMJdtL8NnJkSt4RGuuDNLXVDUpKRc9/4Dkmn/wKYIXKcIiXBNgrpxs2vPSAFeE8XukZHgn6LSJ/oewzI9XIPf3usHxxQr92I98rIAAAAASUVORK5CYII=);
    flex: 1 1 auto;
    display: flex;
    font-family: 'Roboto', sans-serif;
    font-size: 14px;
  }
  
  #root {
    flex: 1 1 auto;
  }
  
  a {
    text-decoration: none;
    color: ${props.blue50};
  }
  
  b {
    font-weight: bold;
  }
  
  html {
    box-sizing: border-box;
  }
  
  *, *:before, *:after {
    box-sizing: inherit;
  }
`