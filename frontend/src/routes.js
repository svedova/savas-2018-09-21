import Async from "./components/Async";
import HomeView from "./views/home";

export const headerRoutes = [
  {
    path: "/docs",
    component: Async(() =>
      import(/* webpackChunkName: "docs" */ "./views/docs/HeaderView")
    )
  }
]

export default [
  {
    path: "/",
    component: HomeView,
    exact: true
  },
  {
    path: "/docs",
    exact: true,
    component: Async(() =>
      import(/* webpackChunkName: "docs" */ "./views/docs")
    )
  },
  {
    path: "*",
    component: Async(() =>
      import(/* webpackChunkName: "error" */ "./views/error")
    )
  }
];
