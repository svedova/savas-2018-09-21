## Disclaimer

This was a fun project - it was a lifetime since I haven't touched Php and was my first time with Laravel. Really liked the framework.

## Requirements

- Php >= 7.1
- docker/docker-compose

## Run the application on your local environment

To install:

```
git clone git@bitbucket.org:svedova/savas-2018-09-21.git
cd savas-2018-09-21
./install.sh
```

To run in dev mode:

```
cd frontend
yarn start

# The browser should open automatically 
# but in case it does not simply visit http://localhost:3000
```

To run in production mode:

```
cd frontend
yarn build

# Now you can visit http://localhost
```

## Features

- Code splitting on frontend
- JWT Authentication (more on this below)
- Dockerized environment
- Store files in filesystem and track them in the database
- Searchable documents
- Remove documents
- Add documents
- Production mode/Dev mode

## Things normally I don't do

- Committing the .env files (here it makes your life easier)
- using browser features like confirm and alert (I'd use a modal normally)
- hardcoding passwords :p

## Missing features

- Eslint/Prettier config
- Commit hooks to run unit tests & linters.

## Security measures:

Disclaimer: Since I am new to Laravel and the level of abstraction is high,
it makes it harder to see the security holes on the backend.

- Apparently Laravel has SQL Injection prevention out of the box. So SQL Injection Prevention.
- The api is protected with JWT Authentication. This prevents unwanted access.
- Queries are written in a way so that user can only fetch their documents.
- React is XSS safe so the app should be safe enough as there is no server side injection.
- If you run `yarn build` and check your http://localhost you will see some headers being added.
  Search for the file `frontend.conf` in `laradock/nginx/sites` to see the headers added.
- In production mode CORS headers should be set.
- No HTTPS implemented since I felt this would be way too much than expected.
- The API Server and the database are publicly reachable. Normally these should lie within a private subnet behind a public load balancer.

## The App

- Frontend and backend are loosely coupled.
- The frontend was bootstrapped with create-react-app. I ejected it as I needed to modify the module aliases.
  I could also remove css-modules since I am using styled-components but left it there as it seems out of scope.
- I have written no tests as it was not asked. But I am very comfortable with unit testing using jest or karma.
  I used to write tests with PHPUnit back in the times, but been a long time so I would need to catch up.
- The backend was bootstrapped with composer. I added a vendor module which handles JWT authentication - so I had to
  add a couple of lines to the config file (but no rocket science, just followed the documentation).
- DocumentController is where most of the logic lies. You can check that file and the Document model.

What works:

- Login
- Adding, removing and listing documents
- Searching documents
- Note: The website was developed on latest chrome and was not tested on different platforms.

What not works:

- Searching documents by type (I felt I already spent too much time on the assignment)
- Downloading documents
- If you use a slow 3G connection you would probably see a crap site as there are no spinners while loading the splitted chunks.
- Same applies for listing the docs. There is a `...` right now.
- Actually I realized this at last moment. I probably could also dockerize the backend so one would not have to install php on their machine.
  Probably you guys have already php on your machine so that would not be a problem.

## Authentication

- The authentication flow is required as the api is session protected. If you run the install script it should
  set up a fake user and the frontend has the credentials hardcoded. All you have to do is simply to press login.
  If you want to clear your session, open your console and type: `localStorage.clear()` and hit enter.

## Known Issues

- .env DB_HOST = postgres works for the api but not for the seeder and migrator. In that case use 127.0.0.1
- JWT's TTL is set to a higher number to avoid adding logic for refreshing the token