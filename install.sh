#!/usr/bin/env bash

set -e

echo "Setting up the environment:"

cd laradock
docker-compose up -d nginx postgres

echo "Environment is ready. Execute 'docker-compose down' to remove the containers when you're done."
echo "Setting up the backend:"

cd ../backend

# Install the dependencies
php composer.phar install

# This is to fix a bug that I did not invest that much time.
sed -i '' -e "s/DB_HOST=postgres/DB_HOST=127\.0\.0\.1/" .env
php artisan migrate
php artisan db:seed
sed -i '' -e "s/DB_HOST=127\.0\.0\.1/DB_HOST=postgres/" .env

echo "Backend is ready. Now preparing the frontend:"

cd ../frontend
yarn