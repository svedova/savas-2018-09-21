<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('TRUNCATE users CASCADE');

        DB::table('users')->insert([
            'email' => 'john.doe@example.com',
            'password' => bcrypt('123456'),
            'name' => 'John Doe',
        ]);
    }
}
