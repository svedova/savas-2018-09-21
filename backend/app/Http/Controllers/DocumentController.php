<?php

namespace App\Http\Controllers;

use Validator;
use App\Document;
use Illuminate\Http\Request;
use \Illuminate\Database\QueryException;
use \Illuminate\Support\Facades\Storage;

class DocumentController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Return all documents which belong to a user.
     */
    public function index() {
        return Document::where('user_id', auth('api')->user()->id)
            ->take(25)
            ->get();
    }

    /**
     * Search documents.
     * 
     * @param Request $request;
     * @return Response
     */
    public function search(Request $request) {
        $type = $request->input('type');
        $query = Document::where('user_id', auth('api')->user()->id)->take(25);

        if ($type) {
            return $query->where('LOWER(`type`)', strtolower($type))->get();
        }

        $title = $request->input('title');

        if ($title) {
            return $query->where('title', 'ilike', "%$title%")->get();
        }

        return [];
    }

    /**
     * Remove the document with the specified id.
     */
    public function remove($id) {
        // The user check is to make sure that a user can
        // delete only their files and not someone elses files.
        // If we would use Document::find($id) users would be able
        // to delete other users files.
        $doc = Document::where('user_id', auth('api')->user()->id)
                       ->where('id', $id)
                       ->first();

        if ($doc) {
            $success = $doc->delete();
            Storage::delete($doc->location);
        } else {
            $success = false;
        }

        return ['success' => $success];
    }

    /**
     * Upload a new document to the server and save the uploaded
     * file information in the database.
     * 
     * @param Request $request;
     * @return Response
     */
    public function upload(Request $request) {
        $messages = [
            'title' => 'Title field is required and should not be longer than 255 chars',
            'doc'   => 'The document is required and allowed types are: jpeg, jpg, png, pdf, txt, doc'
        ];

        $validator = Validator::make($request->all(), [
            'title'   => 'required|array',
            'title.*' => 'required|max:255',
            'doc'     => 'required|array|max:10',
            'doc.*'   => 'required|mimes:jpeg,jpg,png,pdf,txt,doc'
        ], $messages);

        if ($validator->fails()) {
            return ['error' => $validator->messages()];
        }

        $files = $request->file('doc');
        $titles = $request->title;
        $user = auth('api')->user();
        $docs = [];

        for($i = 0; $i < count($files); $i++) {
            $file = $files[$i];
            $title = $titles[$i];
            $doc = new Document();
            $doc->title = $title;
            $doc->user_id = $user->id;
            $doc->type = $file->extension();
            $doc->location = $file->store("docs");

            // Not sure about this one. A query within the for loop
            // might perform bad (though not sure about the underlying tech - perhaps there is a way to keep the connection alive?)
            // But I needed this as on the frontend we need ids to display and delete them.
            // We should be still safe because of the validation max:10.
            if ($doc->save()) {
                $docs[] = $doc;
            }
        }

        return ['success' => true, 'docs' => $docs];
    }   
}
