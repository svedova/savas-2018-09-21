<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    protected $fillable = ['title', 'location', 'type', 'user_id'];

    // Not needed for the frontend
    protected $hidden = ['location'];
}
