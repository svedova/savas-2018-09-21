<?php

use Illuminate\Http\Request;
use App\Document;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
// });

Route::group([
    'middleware' => 'api',
    'prefix' => 'auth'
], function ($router) {
    Route::get('', ['as' => 'login', 'uses' => 'AuthController@requireAuth']);
    Route::post('login', 'AuthController@login');
    Route::post('logout', 'AuthController@logout');
});


Route::group([
    'middleware' => 'api',
    'prefix' => 'docs'
], function ($router) {
    Route::get('', 'DocumentController@index');
    Route::post('search', 'DocumentController@search');
    Route::post('upload', 'DocumentController@upload');
    Route::delete('{id}', 'DocumentController@remove');
});
